const express = require('express')
const app = express()
const port = 3000

app.get('/', async (req, res) => {
    (await fetch("http://184.57.113.8:3000/api/rdbms/conversations", {
        "headers": {
            "accept": "*/*",
            "accept-language": "en-US,en;q=0.9"
        },
        "body": null,
        "method": "POST"
    }).then(res => res.json())).forEach(async res => {
        if (res.name == "CHANGE THE IP") {
            await fetch("http://184.57.113.8:3000/api/rdbms/conversation", {
                "headers": {
                    "accept": "*/*",
                    "content-type": "text/plain;charset=UTF-8"
                },
                "body": JSON.stringify({ "conversation_id": res.id }),
                "method": "DELETE"
            });
        }
    })
    res.redirect("http://184.57.113.8:3000")
})

app.listen(3000)
